jQuery( document ).ready( function( $ ) {


  // -------- global logic ------------- //

  // slide in parent menu
  var slideParentMenu = () => {
    $('.parentMenuBox').toggleClass('slideInParentMenu')
  }

  var slideChildMenu = () => {
    $('.childMenuBox').toggleClass('slideInChildMenu')
  }

  $('#navTrigger').click(function() {
    slideParentMenu()
    slideChildMenu()
  })

  $('.closeX').click(function() {
    slideParentMenu()
    slideChildMenu()
    console.log('test')
  })


  $('.menu-item-has-children').click(function() {
    slideParentMenu()
    let requestedMenu = $(this)[0].id.split("-")[0]
    console.log(requestedMenu)
    $(`#${requestedMenu}-childMenu`).css('display', 'flex')
  })

  $('.childMenuWrap .back').click(function() {
    // slideChildMenu()
    slideParentMenu()
    setTimeout(function() {
      $('ul.childMenu').hide()
    }, 1000)
  })

  //scroll nav trigger
  $('#navTriggerScroll').click(function() {
    slideParentMenu()
    slideChildMenu()
  })

// slide in menu on scroll
// bring in sticky nav on desktop

// let heroHeight = $('.hero').height()
//
// let heightForScrollNav = heroHeight - 100
// console.log(heightForScrollNav)

$(window).scroll(function() {
  if ( $(window).scrollTop() > 80 ) {
    $('.stickyNav').addClass('showStickyNav')

  } else {
    $('.stickyNav').removeClass('showStickyNav')
  }
})

// adds a class to begin animation when element is in viewport
// from https://medium.com/talk-like/detecting-if-an-element-is-in-the-viewport-jquery-a6a4405a3ea2
$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top
  var elementBottom = elementTop + $(this).outerHeight()
  var viewportTop = $(window).scrollTop()
  var viewportBottom = viewportTop + $(window).height()
  return elementBottom > viewportTop && elementTop < viewportBottom
}

$(window).scroll(function() {
  if ($('.line.bottomLine').isInViewport()) {
    $('.line.topLine .crossout').addClass('slideInCrossOut')
    $('.line.bottomLine .crossout').addClass('slideInCrossOut')
  }

  $('.crosshair.animation').each(function() {
    if ($(this).isInViewport()) {
      $(this).addClass('done')
    }
  })

})







});
