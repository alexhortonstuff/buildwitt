<?php

/** Template Name: Home
 *  Description: Home Template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();

    // for now....
    // words to cycle

    $words = [
      'Building',
      'Creating',
      'inspiring',
    ];

    $context['words'] = $words;



    // renders page
    Timber::render('page-home.twig', $context);
