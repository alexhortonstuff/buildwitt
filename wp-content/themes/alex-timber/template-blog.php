<?php

/** Template Name: Blog
 *  Description: Blog page template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();

    global $paged;
    if (!isset($paged) || !$paged){
        $paged = 1;
    }


    // gets the most recent posts
    $blog = array(
      'post_type' => 'post',
      'paged' => $paged,
      'posts_per_page' => 6
    );

    $context['blogs'] = new Timber\PostQuery($blog);
    // $context['blogs'] = Timber::get_posts($blog);



    // renders page
    Timber::render('page-blog.twig', $context);
